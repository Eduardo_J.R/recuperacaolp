Git clone - clona o endereço para um repositório local
git add - adiciona as modificações e arquivos desejados no stage/index
git commit - salva as alterações e faz comentário descrevendo o que foi feito
git pull - atualiza o repositório local
git push - envia as alterações feitas para o repositório remoto
git remote - conecta o repositório a um servidor remoto
git init - inicializa um repositório
git config - configura valores como global ou projeto local 